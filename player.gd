extends CharacterBody2D

enum OCTANTS {
	E = 0,
	SE,
	S,
	SW,
	W,
	NW,
	N,
	NE,
}

const SPEED = 60.0

@onready var sprite := $Sprite
@export var direction := Vector2.UP


func _ready():
	floor_stop_on_slope = true


func cartesian_to_isometric(cartesian: Vector2) -> Vector2:
	return Vector2(cartesian.x - cartesian.y, (cartesian.x + cartesian.y) / 2)


func get_octant(dir: Vector2):
	return wrapi(int(dir.angle() / (PI/4)), 0, 8)



func get_input_direction():
	var dir = Input.get_vector("ui_left", "ui_right", "ui_up", "ui_down")
	if dir == null:
		dir = Vector2.ZERO
	
	return dir


func get_octant_animation_name(animation_name, octant) -> String:
	return animation_name + "_" + OCTANTS.keys()[octant]


func get_animation_from_state(dir, vel):
	var animation_name = "idle" if vel.is_zero_approx() else "run"
	return get_octant_animation_name(animation_name, get_octant(dir))


func update_state(input_dir: Vector2) -> void:
	# Don't reset position if idle
	if not input_dir.is_zero_approx():
		direction = input_dir

	velocity = cartesian_to_isometric(input_dir * SPEED)


func handle_animation():
	sprite.play(get_animation_from_state(direction, velocity))


func _physics_process(_delta):
	var input_dir = get_input_direction()
	
	update_state(input_dir)
	move_and_slide()
	handle_animation()
	
